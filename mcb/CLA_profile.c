//###########################################################################
// Description:
//!  \addtogroup f2806x_example_list
//!  <h1>CLA ADC (cla_adc)</h1>
//!
//! In this example ePWM1 is setup to generate a periodic ADC SOC.
//! Channel ADCINA2 is converted. When the ADC begins conversion,
//! it will assert ADCINT2 which will start CLA task 2.
//!
//! Cla Task2 logs 20 ADCRESULT1 values in a circular buffer.
//! When Task2 completes an interrupt to the CPU clears the ADCINT2 flag.
//!
//! \b Watch \b Variables \n
//! - VoltageCLA       - Last 20 ADCRESULT1 values
//! - ConversionCount  - Current result number 
//! - LoopCount        - Idle loop counter
//
//
//###########################################################################
// $TI Release: F2806x C/C++ Header Files and Peripheral Examples V151 $
// $Release Date: February  2, 2016 $
// $Copyright: Copyright (C) 2011-2016 Texas Instruments Incorporated -
//             http://www.ti.com/ ALL RIGHTS RESERVED $
//###########################################################################

#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
#include "CLAShared.h"
#include "string.h"

int32 cycles_timerStart = 0;    // timer for profiling cycle count
int32 cycles_timerEnd = 0;
int32 cycles_count = 0;




__interrupt void Task_Complete();

#pragma DATA_SECTION(VdcBus_ClaToCpu,"Cla1ToCpuMsgRAM");
float VdcBus_ClaToCpu;

#pragma DATA_SECTION(pwm_counts,"Cla1ToCpuMsgRAM");
Uint32 pwm_counts;

main()
{

// Step 1. Initialize System Control:
// PLL, WatchDog, enable Peripheral Clocks
// This example function is found in the F2806x_SysCtrl.c file.
   InitSysCtrl();

// Step 2. Initialize GPIO:
// This example function is found in the F2806x_Gpio.c file and
// illustrates how to set the GPIO to it's default state.
// InitGpio();  // Skipped for this example

// Step 3. Clear all interrupts and initialize PIE vector table:
// Disable CPU interrupts
   DINT;

// Initialize the PIE control registers to their default state.
// The default state is all PIE interrupts disabled and flags
// are cleared.
// This function is found in the F2806x_PieCtrl.c file.
   InitPieCtrl();

// Disable CPU interrupts and clear all CPU interrupt flags:
   IER = 0x0000;
   IFR = 0x0000;

// Initialize the PIE vector table with pointers to the shell Interrupt
// Service Routines (ISR).
// This will populate the entire table, even if the interrupt
// is not used in this example.  This is useful for debug purposes.
// The shell ISR routines are found in F2806x_DefaultIsr.c.
// This function is found in F2806x_PieVect.c.
   InitPieVectTable();

// Interrupts that are used in this example are re-mapped to
// ISR functions found within this file.
   EALLOW;  // This is needed to write to EALLOW protected register
   PieVectTable.CLA1_INT1 = &Task_Complete;
   PieCtrlRegs.PIEIER11.bit.INTx1 = 1;
   IER |= M_INT11;

   EINT;          						    // Enable Global interrupt INTM
   ERTM;          						    // Enable Global realtime interrupt DBGM
   EDIS;

// Copy CLA code from its load address to CLA program RAM
//
// Note: during debug the load and run addresses can be 
// the same as Code Composer Studio can load the CLA program
// RAM directly. 
// 
// The ClafuncsLoadStart, Cla1funcsLoadSize, and ClafuncsRunStart
// symbols are created by the linker. 
   memcpy(&Cla1funcsRunStart, &Cla1funcsLoadStart, (Uint32)&Cla1funcsLoadSize);    

   // Initialize the CLA registers
   EALLOW;
   Cla1Regs.MVECT1 = (Uint16) ((Uint32)&Cla1Task1 - (Uint32)&Cla1funcsRunStart);
   Cla1Regs.MMEMCFG.bit.PROGE = 1;          // Map CLA program memory to the CLA
   Cla1Regs.MCTL.bit.IACKE = 1;             // Enable IACK to start tasks via software
   Cla1Regs.MIER.all = (M_INT1);   // Enable Task 1

   // Configure CPU-Timer 1 for benchmarking cycle count
   CpuTimer1Regs.TPR.all  = 0;
   CpuTimer1Regs.TPRH.all  = 0;
   CpuTimer1Regs.TCR.bit.TSS = 1;      //Initially Stop Timer
   CpuTimer1Regs.TCR.bit.TRB = 1;      //Reload Timer
   CpuTimer1Regs.TCR.bit.SOFT = 0;
   CpuTimer1Regs.TCR.bit.FREE = 0;     // Timer Free Run Disabled
   CpuTimer1Regs.TCR.bit.TIE = 0;      // 0 = Disable/ 1 = Enable Timer Interrupt
   EDIS;

   // Assumes ePWM1 clock is already enabled in InitSysCtrl();
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
    EDIS;
    EPwm1Regs.ETSEL.bit.SOCAEN   = 1;        // Enable SOC on A group
    EPwm1Regs.ETSEL.bit.SOCASEL  = 4;        // Select SOC from from CPMA on upcount
    EPwm1Regs.ETPS.bit.SOCAPRD   = 1;        // Generate pulse on 1st event
    EPwm1Regs.CMPA.half.CMPA     = 0xFFFF;   // Set compare A value
    EPwm1Regs.TBPRD              = 0xFFFF;   // Set period for ePWM1 - this will determine the sampling frequency
    EPwm1Regs.TBCTL.bit.CTRMODE  = 0;        // count up and start
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    EDIS;

   // Wait for ADC interrupt
      for(;;)
      {
         // Time/Profile Module
         CpuTimer1Regs.TCR.bit.TSS = 1; // Stop Timer
         cycles_timerStart = CpuTimer1Regs.TIM.all; // Get timer start value
         CpuTimer1Regs.TCR.bit.TSS = 0; // Startup Timer
         Cla1ForceTask1(); // Jump to routine to be profiled
         __asm(" IDLE");
         CpuTimer1Regs.TCR.bit.TSS = 1; // Stop Timer
         cycles_timerEnd = CpuTimer1Regs.TIM.all; // Get timer end value

         cycles_count = cycles_timerStart - cycles_timerEnd; // Cycle count, account for jump
      }

}

//==================================================================================
// TASK COMPLETE TRIGGER
//==================================================================================
__interrupt void Task_Complete()
{
    // Clear interrupt flag
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP11;
}

