//###########################################################################
//
// FILE:   CLAShared.h
//
// TITLE:  CLA and CPU shared variables and constants
//
//###########################################################################
// $TI Release: F2806x C/C++ Header Files and Peripheral Examples V151 $
// $Release Date: February  2, 2016 $
// $Copyright: Copyright (C) 2011-2016 Texas Instruments Incorporated -
//             http://www.ti.com/ ALL RIGHTS RESERVED $
//###########################################################################


#ifndef CLA_SHARED_H
#define CLA_SHARED_H

#ifdef __cplusplus
extern "C" {
#endif

#include "DSP28x_Project.h"

#define READ_CLOCK(X) __meallow();\
                        EPwm1Regs.TBCTL.bit.CTRMODE = TB_FREEZE;\
                        X = EPwm1Regs.TBCTR;\
                      __medis();
#define RESTART_CLOCK __meallow();\
                        EPwm1Regs.TBCTL.bit.CTRMODE = TB_FREEZE;\
                        EPwm1Regs.TBCTR = 0;\
                        EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;\
                      __medis();



extern float VdcBus_ClaToCpu;
extern Uint32 pwm_counts;

__interrupt void Cla1Task1();

// These are defined by the linker file
extern Uint16 Cla1funcsLoadStart;
extern Uint16 Cla1funcsLoadEnd;
extern Uint16 Cla1funcsRunStart;
extern Uint16 Cla1funcsLoadSize;

#ifdef __cplusplus
}
#endif /* extern "C" */


#endif  // end of CLA_SHARED definition
